# Patch Friends

You can play it at [patchfriends.org](https://patchfriends.org) or on [gitlab.io](https://aquarichy.gitlab.io/patch-friends/).

Patch Friends is a cute demo of a classic RPG that runs in your browser.
It uses the [PAYGE game engine](https://gitlab.com/aquarichy/payge) and the [Patch Flap tileset](https://gitlab.com/aquarichy/patch-flap-tileset).
Patch Friends provides game data to be used with the PAYGE engine to tell a specific story.

Patch Friends was inspired by a request by a good friend to draw their cats as video game characters.
I did, and went overboard and made a playable mini-game where she could interact with them.
It also serves as a tech demo for my portfolio.

Features:

- tile-based map that you can explore (go beyond the edge to move to a new screen)
- "teleports": entering a door takes you inside
- dialogue and menu system for NPCs and battle
- interactive NPCs: talk to them
- interactive objects: bump against a sign, a painting, a vending machine, etc. to interact
- battle system: send forth your party members (Patch Friends) to do battle in random encounters

If you'd like to see any additional features, let me know.

## Screenshots

Make new friends (with one minimally scripted NPC)!

<a target="_blank" href="doc/1.bw.talk menu.png"><img src="doc/1.bw.talk menu.png" width="400px" /></a>

See the world in Patchnicolour!

<a target="_blank" href="doc/2.colour.png"><img src="doc/2.colour.png" width="400px" /></a>

Trespass random buildings!

<a target="_blank" href="doc/3.indoors.png"><img src="doc/3.indoors.png" width="400px" /></a>

Let your Patch Friends hug out their differences!

<a target="_blank" href="doc/4.battle.png"><img src="doc/4.battle.png" width="400px" /></a>


## Installation

You can build an OCI container image for the project using the Containerfile
provided, or install the files directly.  A Makefile is provided to help with either approach.

If you have the podman container manager installed, you can do:

> $ make oci_build

> $ make oci_run

If you prefer docker, you may need to rename Containerfile to Dockerfile.

Otherwise, you can look at the Makefile and its variables and override them in
vars.mk to suit your own situation. By default, it assumes a tileset
(patch-flap) and engine (PAYGE) will be available in /tmp/ (!). In particular,
it assumes the files will be the output from 'make dist' called on both of those
projects.

## License

GNU GPLv3

### Credits and Copyright

- Yuki DP and Miku DP designs copyright [Deanna Pinder](https://www.instagram.com/withasideofpaint/) ©2023
- Code and other Patch Friends by Richard Schwarting (me)
- mulberry32 RNG [implementation](https://stackoverflow.com/questions/521295/seeding-the-random-number-generator-in-javascript) by SO user [bryc](https://stackoverflow.com/users/815680/bryc)
