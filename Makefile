INST_HTML_DIR=/tmp/patchfriends

# To install an engine (like PAYGE) or a tile set (like Patch-Flap) using this Makefile, the engine/tile set must:
# - file: provide a compress tar balls (e.g. .tar.gz) that can be dropped into engine/ or tileset/
# - git: provide a Makefile with a 'dist' target that generates a compressed tar ball like the one used by file.
# Realistically, Patch Friends will probably only ever use PAYGE, but an alternate tile set is plausible.
# Values for *_SRC_TYPE and _SRC_PATH:
# E.g.
#   ENGINE_SRC_TYPE: file, git
#   ENGINE_SRC_PATH: e.g. /tmp/$(TILESET_NAME), https://gitlab.com/user/payge.git
ENGINE_SRC_TYPE=file
ENGINE_NAME=payge
ENGINE_SRC_PATH=/tmp/$(ENGINE_NAME)
ENGINE_BUILD_CMD=make
ENGINE_FILE_NAME=payge.tar.gz

TILESET_SRC_TYPE=file
TILESET_NAME=patch-flap
TILESET_SRC_PATH=/tmp/$(TILESET_NAME)
TILESET_BUILD_CMD=make
TILESET_FILE_NAME=patch-flap.tar.gz

OCI_BIN=podman
OCI_IMAGE_TAG=localhost/patch-friends
OCI_HOST_PORT=8080

# should redefine INST_DIR
-include vars.mk

all: engine/$(ENGINE_NAME) tileset/$(TILESET_NAME)

$(INST_HTML_DIR):
	test -d "$(INST_HTML_DIR)" || mkdir "$(INST_HTML_DIR)"

engine/$(ENGINE_NAME):
	test -d "engine" || mkdir "engine"
	if [ "$(ENGINE_SRC_TYPE)" == "file" ]; then \
		cp -pr "$(ENGINE_SRC_PATH)" "engine/"; \
		cd "engine" && tar -xf "$(ENGINE_FILE_NAME)"; \
	elif [ "$(ENGINE_SRC_TYPE)" == "git" ]; then \
		if [ -d "cache/engine/$(ENGINE_NAME)/.git" ]; then \
			( \
				cd "cache/engine/$(ENGINE_NAME)"; \
				git fetch; \
			); \
		else \
			mkdir -p cache/engine; \
			git clone "$(ENGINE_SRC_PATH)" cache/engine/"$(ENGINE_NAME)"; \
		fi; \
		( \
			cd cache/engine/"$(ENGINE_NAME)"; \
			make dist; \
		); \
		cd engine; \
		tar -xf "../cache/engine/$(ENGINE_NAME)/dist/$(ENGINE_FILE_NAME)"; \
	fi

tileset/$(TILESET_NAME):
	test -d "tileset" || mkdir "tileset"
	if [ "$(TILESET_SRC_TYPE)" == "file" ]; then \
		cp -pr "$(TILESET_SRC_PATH)" "tileset/"; \
		cd "tileset" && tar -xf "$(TILESET_FILE_NAME)"; \
	elif [ "$(TILESET_SRC_TYPE)" == "git" ]; then \
		if [ -d "tileset/$(TILESET_NAME)/.git" ]; then \
			( cd "tileset/$(TILESET_NAME)"; \
			  git fetch; ) \
		else \
			mkdir -p cache/tileset; \
			git clone "$(TILESET_SRC_PATH)" "cache/tileset/$(TILESET_NAME)"; \
		fi; \
		( cd "cache/tileset/$(TILESET_NAME)"; \
		  make dist; ); \
		cd tileset; \
		tar -xf "../cache/tileset/$(TILESET_NAME)/dist/$(TILESET_FILE_NAME)"; \
	fi

dist: all
	echo -n "";  # TODO: have us generate a .tar.gz first and then let install use that

install_engine: engine/$(ENGINE_NAME) $(INST_HTML_DIR)
	cp -prd "engine/$(ENGINE_NAME)/html/"* "$(INST_HTML_DIR)/"

install_tileset: tileset/$(TILESET_NAME) $(INST_HTML_DIR)
	test -d "$(INST_HTML_DIR)/tileset" || mkdir "$(INST_HTML_DIR)/tileset"
	cp -prd "tileset/$(TILESET_NAME)/" "$(INST_HTML_DIR)/tileset/$(TILESET_NAME)"
	cd "$(INST_HTML_DIR)/tileset" && (test -L "active" || ln -s "$(TILESET_NAME)" "active")

install: $(INST_HTML_DIR) install_engine install_tileset
	cp -prd data    "$(INST_HTML_DIR)/"
	cp -prd ui      "$(INST_HTML_DIR)/"

uninstall:
	mv "$(INST_HTML_DIR)" "$$(mktemp -d)"/

oci_build:
	$(OCI_BIN) build . -t $(OCI_IMAGE_TAG)

oci_run:
	$(OCI_BIN) run -dt -p "$(OCI_HOST_PORT):80" $(OCI_IMAGE_TAG)
	@echo "Visit http://localhost:$(OCI_HOST_PORT) to run"

clean:
	find . -name "*~" | xargs -I FILE rm "FILE"
	rm -rf tileset/*
	rm -rf engine/*
	rm -rf cache/
