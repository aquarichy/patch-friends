const my_teleports = {
  /* from Habit Town (100,81) to Habit Town - House 1 (100,180) */
  "107,83":  { map_start:    { x: 100, y: 180 },
               player_start: { x: 104, y: 187 } },
  /* from Habit Town - House 1 (100,180) to Habit Town (100,81) */
  "104,187": { map_start:    { x: 100, y:  81 },
               player_start: { x: 107, y:  83 } },

  /* from Habit Town (100,81) to Habit Town - House 2 (110,180) */
  "107,89":  { map_start:    { x: 110, y: 180 },
               player_start: { x: 114, y: 187 } },
  /* from Habit Town - House 2 (110,180) to Habit Town (100,81) */
  "114,187": { map_start:    { x: 100, y:  81 },
               player_start: { x: 107, y:  89 } },

  /* Habit Town Café */
  /*   Outside door entering */
  "99,83": { map_start: { x: 90, y: 189 },
             player_start: { x: 94, y: 196 } },
  /*   Inside south door to outside */
  "94,196": { map_start: { x: 90, y: 81 },
              player_start: { x: 99, y: 83 } },
  "94,197": { map_start: { x: 90, y: 81 },
              player_start: { x: 99, y: 83 } },
  /*   Inside north door to washroom */
  "94,180": { map_start: { x: 90, y: 180 },
              player_start: { x: 94, y: 181 } },
};
