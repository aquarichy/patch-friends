const my_shops = {
  "95,192": {
    "☕ Tea": {    action: (item, cb) => { message_log (`You drank a cup of ${item}.  You feel calm.`, cb); } },
    "🍪 Cookie": { action: (item, cb) => { message_log (`You shared a ${item} with ${window.my_friend.name}.  You feel happy.`,
                                                        () => { window.my_friend.heal_full (); cb (); })}},
  },
  "113,183": {
    "🥤 Juice": { action: (item, cb) => {
      message_log (`You drank a litre of ${item}.  You feel hyper now!`, () => { shop_boost_speed (3, 15), cb () });
    }},
    "🍎 Apple": { action: (item, cb) => {
      message_log (`You fed ${window.my_friend.name} an ${item}.  They are filled with health.`,
                   () => { window.my_friend.heal_full (); cb () });
    }},
  },
};
