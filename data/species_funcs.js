function species_setup (species_dict) {
  for (let name in species_dict) {
    const species =  species_dict[name];

    Object.assign (species, {
      name: name,
      src: `data/species/${name}/64.png`,
      cry_src: `data/species/${name}/cry.wav`,
      version: 3,
    });
  }

  return species_dict;
}
