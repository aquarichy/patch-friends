/* for vending machine */
function shop_boost_speed (factor, time_s) {
  const player = document.body.querySelector ("#player");
  player.speed *= factor;
  setTimeout (function () {
    player.speed /= factor;
  }, time_s * 1000);
}
