const my_signs = {
  /* in Habit Town (100,81) */
  "101,87": function ()  {
    const f = window.my_friend;
    const img_style = `position: absolute; width: 15cqw; top: 3cqw; right: 1cqw; filter: opacity(0.2)`;
    return `Name:    <strong>${f.name}</strong>\n`+
      (f.name != f.species.name ? `Species: ${f.species.name}\n` : "")+
      `Type(s):\n  `+(f.types.join (", "))+`\n`+
      `\n` + 
      `Level:   ${f.level}, Exp ${f.exp}\n`+
      `HP:      ${f.hp_cur}/${f.hp_max}\n`+
      `Attack:  ${f.attack}\n`+
      `Defense: ${f.defense}\n\n`+
      `<img style="${img_style}" src="${f.species.src}" />`+
      `Level-Up Modifiers:\n`+
      `HP  `+(Math.round (f.hp_mod * 100)/100)+`\n`+
      `ATT `+(Math.round (f.attack_mod * 100)/100)+`\n`+
      `DEF `+(Math.round (f.defense_mod * 100)/100);
  },
  "105,83": "WELCOME TO HABIT TOWN\n\nMayor D is so smart and fun!  She's a great trainer, healther, fooder, athleticer, painter, and friender!",
};
