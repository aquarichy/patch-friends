
const g_game_title_html = `PATCH<img style="height: 1em; padding-right: 4px;" src="ui/logo.png">FRIENDS`;
const g_credits_html = `
  <li><a href="https://gitlab.com/aquarichy/patch-friends">Patch Friends</a> (gitlab) created by <a href="https://kosmokaryote.org">Richard Schwarting</a></li>
  <li>Miku DP and Yuki DP: <a href='https://www.instagram.com/withasideofpaint/'>Deanna Pinder (instagram)</a></li>
  <li>Sylver: Concept by Sylvester Stalletto</li>
  <li><a href="https://gitlab.com/aquarichy/patch-flap-tileset">Patch Flap tileset (gitlab)</a></li>
`;

window.addEventListener ("load", (_ev) => {
  Object.assign (document.body.style, {
    backgroundImage: "url(ui/body_bg.png)",
    backgroundSize: "cover",
    backgroundPosition: "center",
    backdropFilter: "contrast(0.6)",
  });
});
