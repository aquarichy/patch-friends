const my_moves = {
  /* General moves */
  "Bite": {
    types: [ "General" ],
    mod: 1.2,
    action: function (move, cb) { this.default_attack (move, cb); },
  },
  "Scratch": {
    types: [ "General" ],
    mod: 1,
    action: function (move, cb) { this.default_attack (move, cb); },
  },
  "Growl": {
    types: [ "General" ],
    mod: 0.5,
    action: function (move, cb) { this.default_stat_change (move, cb, 'attack'); },
  },
  "Hiss": {
    types: [ "General" ],
    mod: 0.5,
    action: function (move, cb) {this.default_stat_change (move, cb, 'defense'); },
  },

  /* Fire moves */
  "Boil": {
    types: [ "Fire" ],
    mod: 1,
    action: function (move, cb) { this.default_attack (move, cb); },
  },

  "Iron Tail": {
    types: [ "Fighting", "Metal" ],
    mod: 1.2,
    action: function (move, cb) { this.default_attack (move, cb); },
  },
  "Dream": {
    types: [ "Fae" ],
    mod: 1,
    action: function (move_name, end_turn_cb) {
      const move = data.moves[move_name];

      let message = `${this.attacker.name} used ${move_name} on ${this.target.name}, who had a`;
      let attacker_damage = (this.target.attack / this.attacker.defense) * move.mod * Math.random ();
      let target_damage   = (this.target.attack / this.target.defense  ) * move.mod * Math.random ();

      if (Math.random () < 0.4) {
        message = `${message} nightmare and lashed out, damaging everyone!`;
        attacker_damage /= 2;
        hp_change_func = this.friend_hp_dec.bind (this);
      } else {
        message = `${message} beautiful dream and sees joy in everything, healing everyone!`,
        attacker_damage *= 1;
        target_damage /= 2;
        hp_change_func = this.friend_hp_inc.bind (this);
      }

      message_log (message, () => {
        hp_change_func (this.attacker, attacker_damage, () => {
          hp_change_func (this.target, target_damage, end_turn_cb);
        }); /* target's damage/healing to attacker */
      }); /* message */
    }
  },
  "Disassociate": {
    types: [ "Psychic" ],
    mod: 1.2,
    action: function (move_name, end_turn_cb) {
      let aspects_swapped = [];
      for (let x of [ 'level', 'exp', 'name', 'types', 'hp_max', 'hp_cur', 'attack', 'defense',
                      'hp_mod', 'attack_mod', 'defense_mod', 'moves' ]) {
        if (Math.random () < 0.2) {
          let tmp = this.attacker[x];
          this.attacker[x] = this.target[x];
          this.target[x] = tmp;

          aspects_swapped.push (x);
        }
      }

      message_log (`${this.attacker.name} used ${move_name}.  `+
                   `${aspects_swapped.length} aspect(s) of their identities were swapped!`+
                   ` (` + aspects_swapped.join (", ") + ")",
                   end_turn_cb);
    },
  },
  "Dazzle" : {
    types: [ "Fae" ],
    mod: 0.25,
    action: function (move, cb) { this.default_stat_change (move, cb, 'defense'); },
  },
  "Sky Dance": {
    types: [ "Flying" ],
    mod: 1.2,
    action: function (move, cb) { this.default_attack (move, cb); },
  },
  "Dust Bath": {
    types: [ "Grass" ],
    mod: 2,
    action: function (move, cb) {
      this.attacker.defense_battle_mod *= 2;
      this.attacker.attack_battle_mod *= 2;
      message_log (`${this.attacker.name} used ${move}.  Their attack and defense temporarily doubled!`, cb);
    },
  },
};
