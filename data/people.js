const my_people = {
  0: { /* DP */ },
  1: { go: false,
       name: "Architect",
       src: "5.people/00001.Architect/1.architect.png",
       msg: "Hallo!  Would you like to change your Patch Friend?",
       options: {
         "Miku DP": {},
         "Miku RS": {},
         "Yuki DP": {},
         "Yuki RS": {},
         "Sylver":  {},
         "Nevermind": {
           action: function (option_name, cb) {
             town_message_log ("Okay, maybe next time.", cb);
           }
         }
       },
       default_action: function (species_name, cb) {
         const old_name = window.my_friend.name;
         const new_name = window.prompt ("What would you like to call your new friend?", species_name);

         window.my_friend = new Friend (data['species'][species_name]);
         if (new_name != null) {
           window.my_friend.name = new_name.replace (/[<>"\\]/g, "*");
         }
         town_message_log (`${old_name} has been released back into the wild.\n`+
                           `${window.my_friend.name} has joined you!`, cb);
       },
       bump: function (x,y,cb) {
         people_speak_bump_func (x,y, () => {
           town_menu_new (this.options, cb, () => this.options["Nevermind"].action ('', cb), this.default_action);
         });
       }
     },       
};
