function shop_bump_func (x,y, cb) {
  message_log ("What would you like?", () => {
    town_menu_new (data.shops[`${x},${y}`], cb);
  });
}

function read_sign_bump_func (x,y, cb) {
  const sign_screen = document.querySelector ("DIV#sign-screen");
  const sign = document.querySelector ("DIV#sign");
  const town_screen = document.querySelector ("DIV#town-screen");
  const player = document.body.querySelector ("#player")

  const xy = `${x},${y}`;
  const sign_value = data['signs'][xy];

  if (!sign_value) {
    console.log (`ERROR: no sign found for ${xy}`);
    cb ();
    return;
  }

  if (typeof (sign_value) == "string") {
    sign.innerHTML = sign_value;
  } else if (typeof (sign_value) == "function") {
    sign.innerHTML = sign_value ();
  }

  town_player_lock_inc (player, "read_sign_bump_func");

  sign_screen.style.display = "block";
  town_screen.style.filter = "blur(5px)";

  on_enter (function () {
    sign_screen.style.display = "none";
    town_screen.style.filter = "";
    town_player_lock_dec (player, "read_sign_bump_func");
    sign.innerText = "";

    cb ();
  });
}

function view_painting_bump_func (x, y, cb) {
  const painting_screen = document.querySelector ("DIV#painting-screen");
  const img = document.querySelector ("IMG#painting-img");
  const credit = document.querySelector ("DIV#painting-credit");
  const town_screen = document.querySelector ("DIV#town-screen");
  const player = document.querySelector ("DIV#player");

  const xy = `${x},${y}`;
  const painting = data['paintings'][xy];
  if (!painting) {
    console.log (`ERROR: no painting found for ${xy}`);
    cb ();
    return;
  }

  img.src = `data/paintings/${painting.src}`;
  credit.innerText = painting.credit;

  town_player_lock_inc (player, "view_painting_bump_func");

  town_screen.style.display = "none";
  painting_screen.style.display = "block";

  on_enter (function () {
    painting_screen.style.display = "none";
    town_screen.style.display = "block";
    town_player_lock_dec (player, "view_painting_bump_func");
    img.src = "";
    credit.innerText = "";

    cb ();
  });
}
