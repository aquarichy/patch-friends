const my_paintings = {
  /* paintings in Habit Town - House 1 (100,180) */
  "103,182": { src: "miku.dp.png", /* TODO: consider doing these as 160x144 versions */
               credit: "Miku (fighting × steel), by Deanna Pinder" },
  "104,182": { src: "miku.rs.png",
               credit: "Miku (fire × rock), by Richard Schwarting" },
  "105,182": { src: "yuki.dp.png",
               credit: "Yuki (fairy), by Deanna Pinder" },
  "106,182": { src: "yuki.rs.png",
               credit: "Yuki (psychic), by Richard Schwarting" },
};
