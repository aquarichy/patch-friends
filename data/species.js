const my_species = {
  "Miku DP": {
    hp_mod: 1.1,
    attack_mod: 0.9,
    defense_mod: 1,
    types: [ "Fighting", "Metal" ],
    moves: [ "Scratch", "Iron Tail", "Growl" ],
  },
  "Miku RS": {
    hp_mod: 0.9,
    attack_mod: 1.1,
    defense_mod: 1,
    types: [ "Fire", "Stone" ],
    moves: [ "Bite", "Boil", "Growl" ],
  },
  "Yuki DP": {
    hp_mod: 1.2,
    attack_mod: 0.8,
    defense_mod: 1,
    types: [ "Fae" ],
    moves: [ "Scratch", "Dream", "Hiss" ],
  },
  "Yuki RS": {
    hp_mod: 0.8,
    attack_mod: 1.2,
    defense_mod: 1,
    types: [ "Psychic" ],
    moves: [ "Bite", "Disassociate", "Hiss" ],
  },
  "Sylver": {
    hp_mod: 1.0,
    attack_mod: 1.4,
    defense_mod: 0.6,
    types: [ "Grass", "Flying" ],
    moves: [ "Dazzle", "Sky Dance", "Dust Bath" ],
  }
};
